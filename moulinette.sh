#!/bin/bash   

DATA=$1                             
datafile=$2
export LC_NUMERIC="en_US.UTF-8";

#deltas_file="./config/deltas";


# Create dirs 
 mkdir -p $DATA/stream;
 mkdir -p $DATA/basicstats;
# mkdir -p $DATA/degrees;

# ----------------------------------------------- STREAM ------------------------------------------------ #


#Extraction .dump sous le format t u v avec u et v sous format IP-MAC - IPv4 seulement - dure 23h
if [ -s $DATA/stream/$datafile.raw.gz ]; then
	echo "RAW file exists.";
else
	for d in $DATA/stream/dump/*           
		do
			for f in $d/*
				do
					tcpdump -e -i eth0 -tt -nq -r $f 2>/dev/null | awk '/IPv4/{gsub(/\,/, "", $4); gsub(/\:/, "", $10); print $1, $2, $4, $8, $10}' | awk -F'[. ]' '{if(NF==12){printf "%s.%s %s.%s.%s.%s-%s %s.%s.%s.%s-%s\n", $1, $2, $5, $6, $7, $8, $3, $9, $10, $11, $12, $4}else if(NF==14){printf "%s.%s %s.%s.%s.%s-%s %s.%s.%s.%s-%s\n", $1, $2, $5, $6, $7, $8, $3, $10, $11, $12, $13, $4}else{printf "Error number of fields - line : %s\n", NR; exit}}'| awk -v t0=$t0 '($1>=1251781200)&& ($1<1251782100)' | gzip -c >> $DATA/stream/$datafile.raw.gz
				done
		done

		echo "RAW file done.";
fi


# On prend la partie entière de la première colonne de la première ligne et on soustrait cette dernière à tout les temps.
t0=$(zcat $DATA/stream/$datafile.raw.gz|awk -F '[. ]' '{print $1;exit}')

if [ -s $DATA/stream/$datafile.t0.directed.gz ]; then
	echo "T0 file exists.";
else
	zcat $DATA/stream/$datafile.raw.gz|awk -v t0=$t0 '{printf("%6f %s %s\n", $1-t0,$2,$3)}'|gzip -c > $DATA/stream/$datafile.t0.directed.gz
	echo "T0 file done.";
fi

# undirected links

if [ -s $DATA/stream/$datafile.t0.gz ]; then
	echo "T0 undirected file exists.";
else
	zcat  $DATA/stream/$datafile.t0.directed.gz | awk '{if($2<$3){print($1,$2,$3);}else{print($1,$3,$2)}}' |gzip -c > $DATA/stream/$datafile.t0.gz
	echo "T0 undirected file done.";
fi


# "t u v" file readable by c programs

zcat $DATA/stream/$datafile.t0.gz | awk '{printf "%s\n%s\n", $2, $3}' | sort -u | awk -f $DATA/scripts/conversion.AWK > $DATA/stream/$datafile.conversion.IP


< zcat $DATA/stream/$datafile.t0.gz awk 'FNR==NR{a[$1]=$2; next}{$2=a[$2]; $3=a[$3]}1' $DATA/stream/$datafile.conversion.IP - | gzip -c > $DATA/stream/$datafile.t0.IP.converted.gz
zcat $DATA/stream/$datafile.t0.IP.converted.gz | awk '{f=(1/2)*(($2+$3)*($2+$3)+3*$2+$3); g=(1/2)*(($2+$3)*($2+$3)+$2+3*$3); if(g>f){printf "%s %s %s %s %s\n", $1, $2, $3, f}else{printf "%s %s %s %s %s\n", $1, $2, $3, g}}' > $DATA/stream/tmp1
awk '{print $4}' $DATA/stream/tmp1 |sort -u | awk -f $DATA/scripts/conversion.AWK >  $DATA/stream/tmp2
awk 'FNR==NR{a[$1]=$2; next}{$4=a[$4]}1' tmp2 tmp1 | gzip -c > $DATA/stream/$datafile.t0.IP.IDlink.converted.gz
zcat $DATA/stream/$datafile.t0.IP.IDlink.converted.gz | awk '{print $2, $3, $4}' | sort -u -k3,3n >  $DATA/stream/$datafile.conversion.IDlink



aggregated_links_number=$(zcat $DATA/stream/$datafile.t0.gz | wc -l ) 
distinct_ip_number=$(wc -l $DATA/stream/$datafile.conversion.IP | awk '{print $1}')
distinct_link_number=$(wc -l $DATA/stream/$datafile.conversion.IDlink | awk '{print $1}')

printf "%s\n%s\n%s\n" $aggregated_links_number $distinct_ip_number $distinct_link_number > $DATA/stream/$datafile.t0.c.prog
zcat  $DATA/stream/$datafile.t0.IP.IDlink.converted.gz >> $DATA/stream/$datafile.t0.c.prog
gzip -c $DATA/stream/$datafile.t0.c.prog > $DATA/stream/$datafile.t0.c.prog.gz

rm -f tmp1 tmp2 $DATA/stream/$datafile.t0.c.prog


# Sanity checks + alpha, omega, delta_t.
# checks number of fields, increasing time, trace total time, IP and MAC adresses format 

if [ -s $DATA/config/info.link.stream ]; then
	delta=$(awk 'NR==1{print $1; exit}' $DATA/config/deltas)
	alpha=0
	omega=900
	delta_t=900
	echo "Sanity checks already done.";
else
	delta=$(awk 'NR==1{print $1; exit}' $DATA/config/deltas)
	alpha=0
	omega=900
	delta_t=900
	printf "%s\n%s\n%s\n" $alpha $omega $delta_t > $DATA/config/info.link.stream

	echo "Sanity checks...";

	zcat $DATA/stream/$datafile.t0.gz | awk '{if(NF!=3){printf "\n Incorrect number of fields : line %s\n", NR ; exit}; if(NR>1){if($1<p){printf "\n Increasing time not respected : line %s.\n", NR; exit}}; p=$1}'
	echo "1st check done."

	zcat $DATA/stream/$datafile.t0.gz | awk '{a=match($1,/^[0-9]+\.[0-9]{6}$/) ; b=match($2,/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\-([0-9A-Fa-f]{2}\:){5}([0-9 a-f]{2})$/); c=match($3,/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\-([0-9A-Fa-f]{2}\:){5}([0-9 a-f]{2})$/); if(a==0){printf "Wrong time format : %s, line %s\n", $1, NR ;exit}; if(b==0){printf "Wrong IP format : %s, line %s, 2nd column, time %s.\n", $2, NR, $1 ;exit}; if(c==0){printf "Wrong IP format : %s, line %s, 3nd column, time :%s.\n", $3, NR, $1 ;exit}}' 
	echo "2nd check done."
	echo "Sanity checks done.";
fi




# ------------------------------------------------------ L1 ------------------------------------------------------#

# Number of packets per second

if [ -s $DATA/basicstats/$datafile.pktspersecond ]; then
	echo "Number of packets per second exists.";
else
	zcat $DATA/stream/$datafile.t0.gz | awk -f $DATA/scripts/pktspersecond.AWK > $DATA/basicstats/$datafile.pktspersecond

	sort -k1,1n $DATA/basicstats/$datafile.pktspersecond > $DATA/basicstats/$datafile.pktspersecond.sort
	awk -v delta_t=$delta_t -v n_node=1 -v f=1 -f $DATA/scripts/dist.AWK $DATA/basicstats/$datafile.pktspersecond.sort > $DATA/basicstats/$datafile.pktspersecond.dist
	tac $DATA/basicstats/$datafile.pktspersecond.dist | awk -f $DATA/scripts/ccdf.AWK > $DATA/basicstats/$datafile.pktspersecond.ccdf
	awk -v pas=250 -v start=0 -f $DATA/scripts/histogram.AWK $DATA/basicstats/$datafile.pktspersecond.dist > $DATA/basicstats/$datafile.pktspersecond.histo

	cp ./utils/plot.pktspersecond.gp ./utils/plot.pktspersecond.sh $DATA/basicstats/
	cd $DATA/basicstats/
	./plot.pktspersecond.sh $datafile.pktspersecond 
	cd -

	rm -rf $DATA/basicstats/plot.pktspersecond.sh $DATA/basicstats/plot.pktspersecond.gp    
	echo "Number of packets per second done.";          
fi


# Number of IPs per second

if [ -s $DATA/basicstats/$datafile.ipspersecond ]; then
	echo "Number of IPs per second exists.";
else
	zcat $DATA/stream/$datafile.t0.gz | awk -f $DATA/scripts/ipspersecond.AWK > $DATA/basicstats/$datafile.ipspersecond

	sort -k1,1n $DATA/basicstats/$datafile.ipspersecond > $DATA/basicstats/$datafile.ipspersecond.sort
	awk -v delta_t=$delta_t -v n_node=1 -v f=1 -f $DATA/scripts/dist.AWK $DATA/basicstats/$datafile.ipspersecond.sort > $DATA/basicstats/$datafile.ipspersecond.dist
	tac $DATA/basicstats/$datafile.ipspersecond.dist | awk -f $DATA/scripts/ccdf.AWK > $DATA/basicstats/$datafile.ipspersecond.ccdf
	awk -v pas=250 -v start=0 -f $DATA/scripts/histogram.AWK $DATA/basicstats/$datafile.ipspersecond.dist > $DATA/basicstats/$datafile.ipspersecond.histo

	cp ./utils/plot.ipspersecond.sh ./utils/plot.ipspersecond.gp $DATA/basicstats/
	cd $DATA/basicstats/ 
	./plot.ipspersecond.sh $datafile.ipspersecond 2>/dev/null
	cd - 2>/dev/null

	rm -rf $DATA/basicstats/plot.ipspersecond.sh $DATA/basicstats/plot.ipspersecond.gp
	echo "Number of IPs per second done.";  
fi

#Number of packets per IPS per seconde 
if [ -s $DATA/basicstats/$datafile.number.pkts.per.ip.per.second]; then
	echo "Number of packets per IPS per second exists.";
else
	gcc $DATA/code.tuv/number.pkts.ip.c -o number.pkts.ip.out
	zcat $DATA/stream/$datafile.t0.c.prog.gz | ./number.pkts.ip.out
	mv number.pkts.per.ip.per.second $DATA/basicstats/$datafile.number.pkts.per.ip.per.second	
	echo "Number of packets per IPS per second done.";
fi

#Number of packets per pairs of IPS per seconde 
if [ -s $DATA/basicstats/$datafile.number.pkts.per.pair.ip.per.second ]; then
	echo "Number of packets per pairs of IPS per second exists.";	
else
	gcc $DATA/code.tuv/number.pkts.pair.ip.c -o number.pkts.pair.ip.out
	zcat $DATA/stream/$datafile.t0.c.prog.gz | ./number.pkts.pair.ip.out
	mv number.pkts.per.pair.ip.per.second $DATA/basicstats/$datafile.number.pkts.per.pair.ip.per.second
	echo "Number of packets per pairs of IPS per second done.";	
fi

# median on each second vs number of packets per IPs
if [ -s  $DATA/basicstats/$datafile.median.vs.number.pkts.ip]; then
	echo "Median on each second vs number of packets per IPs file exists.";	
else
	size_max=$(awk '{print $3, $4}' $DATA/basicstats/$datafile.number.pkts.per.ip.per.second | uniq -c | sort -k1,1n | tail -n1 | cut -d" " -f3) 
	echo $size_max > $DATA/stream/$datafile.median.per.sec.vs.number.pkts.pair.ip.program.file
	awk '{print $3, $4}' $DATA/basicstats/$datafile.number.pkts.per.ip.per.second | uniq -c > tmp 
	awk 'FNR==NR{a[$2]=$1; next}{if(NR==1){printf "%s %s %s\n%s %s\n", a[$3], $3, $4, $1, $2}else{if($3==p3){printf "%s %s\n", $1, $2}else{printf "%s %s %s\n%s %s\n", a[$3], $3, $4, $1, $2}}p3=$3}END{printf "%s %s\n", $1, $2}' tmp $DATA/basicstats/$datafile.number.pkts.per.ip.per.second >> $DATA/stream/$datafile.median.per.sec.vs.number.pkts.ip.program.file
	rm -f tmp

	gcc median.per.sec.vs.number.pkts.ip.program.c -o median.per.sec.vs.number.pkts.ip.program.out
	./median.per.sec.vs.number.pkts.ip.program.out < $DATA/stream/$datafile.median.per.sec.vs.number.pkts.ip.program.file
	mv median.per.sec.vs.number.pkts.ip $DATA/basicstats/$datafile.median.per.sec.vs.number.pkts.ip

	echo "Median on each second vs number of packets per IPs file done.";	
fi

# median on each second vs number of packets per pairs IPs
if [ -s $DATA/basicstats/$datafile.median.vs.number.pkts.pair.ip ]; then
	echo "Median on each second vs number of packets per pairs IPs file exists.";	
else
	size_max=$(awk '{print $3, $4}' $DATA/basicstats/$datafile.number.pkts.per.ip.per.second | uniq -c | sort -k1,1n | tail -n1 | cut -d" " -f3) 
	echo $size_max > $DATA/stream/$datafile.median.per.sec.vs.number.pkts.pair.ip.program.file
	awk '{print $3, $4}' $DATA/basicstats/$datafile.number.pkts.per.ip.per.second | uniq -c > tmp 
	awk 'FNR==NR{a[$2]=$1; next}{if(NR==1){printf "%s %s %s\n%s %s\n", a[$3], $3, $4, $1, $2}else{if($3==p3){printf "%s %s\n", $1, $2}else{printf "%s %s %s\n%s %s\n", a[$3], $3, $4, $1, $2}}p3=$3}END{printf "%s %s\n", $1, $2}' tmp $DATA/basicstats/$datafile.number.pkts.per.ip.per.second >> $DATA/stream/$datafile.median.per.sec.vs.number.pkts.pair.ip.program.file
	rm -f tmp

	gcc median.per.sec.vs.number.pkts.pair.ip.program.c -o median.per.sec.vs.number.pkts.pair.ip.program.out
	./median.per.sec.vs.number.pkts.pair.ip.program.out < $DATA/stream/$datafile.median.per.sec.vs.number.pkts.pair.ip.program.file
	mv median.per.sec.vs.number.pkts.pair.ip $DATA/basicstats/$datafile.median.per.sec.vs.number.pkts.pair.ip

	echo "Median on each second vs number of packets per pairs IPs file done.";
fi

rm -f *.out

# median on each node vs number of packets per IPs

# median on each node vs number of packets per pairs IPs
sort -k1,1n -k2,2n $DATA/basicstats/$datafile.number.pkts.per.pair.ip.per.second | awk -f $DATA/scripts/median.per.node.vs.number.pkts.pair.ip.AWK > $DATA/basicstats/$datafile.median.per.node.vs.number.pkts.pair.ip


:<<'END'

#Make delta stream

while read delta
do
	delta_sur_2=$(echo $delta/2 | bc)

	if [ -s $DATA/stream/$datafile.t0v.$delta.gz ]; then
		echo "Links stream file exists.";
	else
		zcat $DATA/stream/$datafile.t0.gz | awk '{if($2<$3){print($1,$2,$3);}else{print($1,$3,$2)}}' | sort -T. -k2,2 -k3,3 -k1,1g | awk -v delta=$delta_sur_2 -f $DATA/utils/variation.AWK | sort -T. -k1,1g | gzip -c > $DATA/stream/$datafile.t0v.$delta.gz
		echo "Links stream file exists.";
	fi

	# generate file readable by c programs
	if [ -s $DATA/degrees/$datafile.degprofile.$delta.gz ]; then
		echo "Input c programs done";
	else
		zcat $DATA/stream/$datafile.t0v.$delta.gz | awk '{printf "%s %s %s %s\n%s %s %s %s\n", $1, $2, $3, $4, $1, $2, $4, $3}' | sort -T. -k3,3 -k1,1g | tee $DATA/stream/test1 | awk -v omega=$omega -v alpha=$alpha -f $DATA/scripts/profile.AWK | gzip -c > $DATA/degrees/$datafile.degprofile.$delta.gz
		zcat $DATA/degrees/$datafile.degprofile.$delta.gz | awk '{print $1}' | uniq | awk -f $DATA/scripts/conversion.AWK > $DATA/stream/$datafile.conversion.IP.$delta
	
		zcat $DATA/degrees/$datafile.degprofile.$delta.gz | awk -f $DATA/scripts/capacities.AWK > $DATA/stream/$datafile.capacities.$delta
		sort -k1,1g $DATA/stream/test1 | awk '!seen[$1,$2,$3,$4]++ && !seen[$1,$2,$4,$3]{printf "%s %s %s %s\n", $1, $2, $3, $4}' > $DATA/stream/$datafile.links.not.converted.$delta
		

		awk 'FNR==NR{a[$1]=$2; next}{$3=a[$3]; $4=a[$4]}1' $DATA/stream/$datafile.conversion.IP.$delta $DATA/stream/$datafile.links.not.converted.$delta > $DATA/stream/$datafile.links.converted.$delta
		
		awk '{f=(1/2)*(($3+$4)*($3+$4)+3*$3+$4); g=(1/2)*(($3+$4)*($3+$4)+$3+3*$4); if(g>f){printf "%s %s %s %s %s\n", $1, $2, $3, $4, f}else{printf "%s %s %s %s %s\n", $1, $2, $3, $4, g}}' $DATA/stream/$datafile.links.converted.$delta > $DATA/stream/$datafile.links.ID.$delta
		awk '{print $5}' $DATA/stream/$datafile.links.ID.$delta |sort -u | awk -f $DATA/scripts/conversion.AWK >  $DATA/stream/$datafile.conversion.IDlink.$delta
		awk 'FNR==NR{a[$1]=$2; next}{$5=a[$5]}1' $DATA/stream/$datafile.conversion.IDlink.$delta $DATA/stream/$datafile.links.ID.$delta > $DATA/stream/$datafile.links.ID.converted.$delta

		m=$(wc -l $DATA/stream/$datafile.links.converted.$delta | awk '{print $1}') 
		n=$(wc -l $DATA/stream/$datafile.conversion.IP.$delta | awk '{print $1}')
		n_dlink=$(wc -l $DATA/stream/$datafile.conversion.IDlink.$delta| awk '{print $1}')

		echo $m > $DATA/stream/$datafile.slect.$delta
		echo $n >> $DATA/stream/$datafile.slect.$delta
		echo $n_dlink >> $DATA/stream/$datafile.slect.$delta
		cat $DATA/stream/$datafile.capacities.$delta $DATA/stream/$datafile.links.ID.converted.$delta >> $DATA/stream/$datafile.slect.$delta
	
		echo "Input c programs done";
	fi



	# Number of links with delta
	if [ -s $DATA/basicstats.linkstream/$datafile.nblinks.$delta ]; then
		echo "Number of links"
	else
		gcc $DATA/code.link.stream/number.of.links.c $DATA/code.link.stream/lecture_graph.c -o $DATA/basicstats.linkstream/links.out
		cd $DATA/basicstats.linkstream
		./links.out < ../stream/$datafile.slect.$delta
		mv links.dat $datafile.nblinks.$delta
		cd -
		echo "Number of links"
	fi

	if [ -s $DATA/basicstats.linkstream/$datafile.nblinks.$delta.dist ]; then	
		echo "Number of links stats"
	else
	
		sort -k2,2n $DATA/basicstats.linkstream/$datafile.nblinks.$delta |awk '{print $2,$1}' > $DATA/basicstats.linkstream/$datafile.nblinks.$delta.sort
		awk -v p=100 -v d=1.000000 -v delta_t=$delta_t 'BEGIN{p1=0}{if($1<=p1+p){sum+=d}else{while($1>p1+p){print (p1+p1+p)/2, sum/delta_t; p1=p1+p; sum=0}; sum=d}}END{print (p1+p1+p)/2, sum/delta_t}' $DATA/basicstats.linkstream/$datafile.nblinks.$delta.sort > $DATA/basicstats.linkstream/$datafile.nblinks.$delta.histo
		cat $DATA/basicstats.linkstream/$datafile.nblinks.$delta |cut -d" " -f2|./utils/dist > $DATA/basicstats.linkstream/$datafile.nblinks.$delta.dist
		cat $DATA/basicstats.linkstream/$datafile.nblinks.$delta |cut -d" " -f2|./utils/dist|./utils/ccdf > $DATA/basicstats.linkstream/$datafile.nblinks.$delta.ccdf

		#cp ./utils/plot.links.sh ./utils/plot.links.gp $DATA/basicstats.linkstream/
		#cd $DATA/basicstats.linkstream/ 
		#./plot.links.sh $datafile.links.$delta 2>/dev/null
		#cd -

		#rm -rf $DATA/basicstats.linkstream/plot.links.sh $DATA/basicstats.linkstream/plot.links.gp	
		echo "Number of links stats"
	fi




	# Number of nodes
	if [ -s $DATA/basicstats.linkstream/$datafile.nbnodes.$delta ]; then
		echo "Number of node"
	else
		gcc $DATA/code.link.stream/number.of.nodes.c $DATA/code.link.stream/lecture_graph.c -o $DATA/basicstats.linkstream/node.out
		cd $DATA/basicstats.linkstream
		./node.out < ../stream/$datafile.slect.$delta
		mv node.dat $datafile.nbnodes.$delta
		cd -
		echo "Number of nodes"
	fi

	if [ -s $DATA/basicstats.linkstream/$datafile.nbnodes.$delta.dist ]; then	
		echo "Number of nodes stats"
	else
	
		sort -k2,2n $DATA/basicstats.linkstream/$datafile.nbnodes.$delta |awk '{print $2,$1}' > $DATA/basicstats.linkstream/$datafile.nbnodes.$delta.sort
		awk -v p=100 -v d=1.000000 -v delta_t=$delta_t 'BEGIN{p1=0}{if($1<=p1+p){sum+=d}else{while($1>p1+p){print (p1+p1+p)/2, sum/delta_t; p1=p1+p; sum=0}; sum=d}}END{print (p1+p1+p)/2, sum/delta_t}' $DATA/basicstats.linkstream/$datafile.nbnodes.$delta.sort > $DATA/basicstats.linkstream/$datafile.nbnodes.$delta.histo
		cat $DATA/basicstats.linkstream/$datafile.nbnodes.$delta |cut -d" " -f2|./utils/dist > $DATA/basicstats.linkstream/$datafile.nbnodes.$delta.dist
		cat $DATA/basicstats.linkstream/$datafile.nbnodes.$delta |cut -d" " -f2|./utils/dist|./utils/ccdf > $DATA/basicstats.linkstream/$datafile.nbnodes.$delta.ccdf

		#cp ./utils/plot.node.sh ./utils/plot.node.gp $DATA/basicstats.linkstream/
		#cd $DATA/basicstats.linkstream/ 
		#./plot.node.sh $datafile.node.$delta 2>/dev/null
		#cd -

		#rm -rf $DATA/basicstats.linkstream/plot.node.sh $DATA/basicstats.linkstream/plot.node.gp	
		echo "Number of nodes stats"
	fi




	# Variation around degrees
	if [ -s $DATA/degrees/$datafile.degprofile.$delta.dist ]; then
		echo "Degree distribution in stream.";
	else
		zcat $DATA/degrees/$datafile.degprofile.$delta.gz | sort -T. -k2,2n | awk -v n_node=$n_node -v delta_t=$delta_t -f $DATA/scripts/dist.AWK > $DATA/degrees/$datafile.degprofile.$delta.dist

		tac $DATA/degrees/$datafile.degprofile.$delta.dist | awk -f $DATA/scripts/ccdf.AWK | tac > $DATA/degrees/$datafile.degprofile.$delta.ccdf

		zcat $DATA/degrees/$datafile.degprofile.$delta.gz | awk -f $DATA/scripts/nodemaxdegree.AWK | sort -T. -k2,2n > $DATA/degrees/$datafile.nodemaxdegree.$delta

		echo "Degree distribution in stream.";
	fi

:<<'END'
	# events removal
	for e in ./events/02-*           
	do
		echo "Managing event " $(basename $e);
		wave=$(sed '1q;d' $e);
		file=$(sed '2q;d' $e);
		t1=$(awk 'NR==3{print $1}' $e) 
		t2=$(awk 'NR==3{print $2}' $e) 
		n1=$(awk 'NR==3{print $3}' $e) 

		if [ -s $DATA/stream/$datafile.t0.$wave.gz ]; then
			echo "event" $wave
		else
			zcat $DATA/stream/$file.gz | awk -v t1=$t1 -v t2=$t2 -v n1=$n1 '!(($1>t1 && $1<t2)&&($2==n1 || $3==n1))' | gzip -c > $DATA/stream/$datafile.t0.$wave.gz
		fi


		if [ -s $DATA/basicstats/$datafile.$wave.ipspersecond ]; then
			echo "Number of ips per second." $(basename $e);
		else
			zcat $DATA/stream/$datafile.t0.$wave.gz |python ./scripts/ips-per-bin.py > $DATA/basicstats/$datafile.$wave.ipspersecond
		fi

	done
#END

done <$deltas_file

#clean *~ file 
rm -f *~
END

# ---------------A faire ------------------------ #
#faire une boucle de calcul sur une propriété en enlevant les évènements trouvés jusquà ce qu'on n'en trouve plus 
#ensuite on en calcule une nouvelle et on enlève les évènements de la même façon, et on reprend les calculs depuis le début
# ----------------------------------------------- #


