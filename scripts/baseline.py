import sys
import pdb
import math
RES = "/home/tiphaine/Development/ipstream/results/packets/"

print(sys.argv)


seconds = {}
ips = {}

# load first file (options -k= -v= ?)
first_file = sys.argv[1]
first_key = int(sys.argv[2]) - 1
first_val = int(sys.argv[3]) - 1

seconds_file = open(RES + first_file)
for line in seconds_file:
    contents = line.split(" ")
    s = contents[first_key]
    p = int(contents[first_val])
    seconds[s] = p

# load second file and apply transformation
second_file = sys.argv[4]
second_key = int(sys.argv[5]) - 1 # Give range for key ?
second_val = int(sys.argv[6]) - 1
ops = sys.argv[7]
with open(RES + second_file) as f:
    div = lambda a,b: a/b 
    mult = lambda a,b: a*b
    power = lambda a,b: a**b
    log = lambda a,b: a*math.log(b)
    minus = lambda a,b: a-b
    add = lambda a,b: a+b
    absdiff = lambda a,b: abs(a-b)

    if ops == "add":
        operator = add
    if ops == "abs":
        operator = absdiff
    if ops == "div":
        operator = div
    if ops == "mult":
        operator = mult
    if ops == "minus":
        operator = minus
    if ops == "log":
        operator = log
    out_file = open(RES + first_file + "-" + ops + "-" + second_file, "w")
    for line in f:
        contents = line.split(" ")
        ip = contents[0]
        s = contents[second_key]
        p = int(contents[second_val])

        out_file.write("%f\n" % (operator(p, seconds[s])))

