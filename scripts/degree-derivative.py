import sys


old_d = -1
old_u = ""

for line in sys.stdin:
	contents = line.split(" ")
	u = contents[0].strip()
	d = int(contents[1])
	b = float(contents[2])
	e = float(contents[3])

	if u != old_u and old_u != "":
		old_d = 0.0
	else:
		if old_d != -1:
			sys.stdout.write(u + " " + str(d - old_d) + "\n")
	old_u = u
	old_d = d
