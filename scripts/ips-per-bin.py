import sys

old_t = 0
nb_pkts = 0

ips = set()

for line in sys.stdin:
	contents = line.split(" ")
	t = int(float(contents[0]))
	u = contents[1].strip()
	v = contents[2].strip()

	if old_t != t and old_t != "":
		sys.stdout.write(str(old_t) + " " + str(len(ips)) + "\n")
		old_t = t
		nb_pkts = 0
		ips = set()

	nb_pkts += 1
	ips.add(u)
	ips.add(v)

sys.stdout.write(str(old_t) + " " + str(len(ips)) + "\n")
