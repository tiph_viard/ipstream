import sys

old_t = 0
nodes = set()

for line in sys.stdin:
	t = int(float(line.split(" ")[0]))
        u = str(line.split(" ")[1])
        v = str(line.split(" ")[2]).strip()

	if old_t != t and old_t != "":
		sys.stdout.write(str(old_t) + " " + str(len(nodes)) + "\n")
		old_t = t
		nodes = set()
	nodes.add(u)
        nodes.add(v)

sys.stdout.write(str(old_t) + " " + str(len(nodes)) + "\n")
