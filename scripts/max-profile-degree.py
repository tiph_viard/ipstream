# Outputs nodes with the max degree they attain, the duration over which they attain it, and the average degree of the node
# For all nodes
import sys


total_dur = float(sys.argv[1])
max_deg = 0
(max_b, max_e) = (0,0)
avdeg = 0.0
old_u = ""

for line in sys.stdin:
	contents = line.split(" ")
	u = contents[0].strip()
	d = int(contents[1])
	b = float(contents[2])
	e = float(contents[3])

	if u != old_u and old_u != "":
		avdeg = (avdeg/total_dur)
		sys.stdout.write(str(old_u) + " " + str(max_deg) + " " + str(max_b) + " " + str(max_e) + " " + str(avdeg) + "\n")
		max_deg = 0
		(max_b, max_e) = (0,0)
		avdeg = 0.0

	avdeg += d * (e - b)
	if d > max_deg:
		max_deg = d
		max_b = b
		max_e = e
	elif d == max_deg:
		# Replace if duration is longer
		if e - b > max_e - max_b:
			max_b = b
			max_e = e 
	
	old_u = u

avdeg = (avdeg/total_dur)
sys.stdout.write(str(old_u) + " " + str(max_deg) + " " + str(max_b) + " " + str(max_e) + " " + str(avdeg) + "\n")
