# Median smoothing method
import sys
import numpy
from collections import deque

values = deque()
medsize = int(sys.argv[1])
median_val = 0
old_median_val = None

def median(lst):
    return numpy.median(numpy.array(lst))

for line in sys.stdin:
	contents = line.split(" ")
	t = int(contents[0])
	v = float(contents[1])

	if len(values) >= medsize:
		median_val = median(values)

		if old_median_val is None:
			median_variation = 0
		else:
			median_variation = median_val - old_median_val

		sys.stdout.write("%d %f %f %f\n" % (t - medsize, v, median_val, median_variation))
		values.popleft()
		old_median_val = median_val

	values.append(v)
