import sys

active_nodes = set()
old_nodes = set()
lifes = {}
old_t = -1

for line in sys.stdin:
    contents = line.split(" ")
    t = int(float(contents[0]))
    arr = int(contents[1]) # +1/-1
    u = contents[2].strip()
    v = contents[3].strip()

    if old_t != t:
        n = len(old_nodes) - len(active_nodes)
        old_t = t
        print(str(old_t) + " " + str(n))
        old_nodes = active_nodes.copy()

    if arr == 1:
        try:
            lifes[u] += 1
        except KeyError:
            lifes[u] = 1
        try:
            lifes[v] += 1
        except KeyError:
            lifes[v] = 1
        active_nodes.add(u)
        active_nodes.add(v)

    elif arr == -1:
        lifes[u] -= 1
        lifes[v] -= 1

        if lifes[u] <= 0:
            if u in active_nodes:
				active_nodes.remove(u)
        if lifes[v] <= 0:
            if v in active_nodes:
				active_nodes.remove(v)
