import sys

active_nodes = set()
active_links = set()
lifes = {}
old_t = -1

for line in sys.stdin:
    contents = line.split(" ")
    t = int(float(contents[0]))
    arr = int(contents[1]) # +1/-1
    u = contents[2].strip()
    v = contents[3].strip()
    link = frozenset([u,v])

    if old_t != t:
        n = len(active_nodes)
        m = len(active_links)
        old_t = t
        if n > 1:
            print(str(old_t) + " " + str(n) + " " + str(float(2*m)/(n*(n-1))))
        else:
            print(str(old_t) + " " + str(n) + " 0.0")

    if arr == 1:
        try:
            lifes[u] += 1
        except KeyError:
            lifes[u] = 1
        try:
            lifes[v] += 1
        except KeyError:
            lifes[v] = 1
        active_nodes.add(u)
        active_nodes.add(v)

        try:
            lifes[link] += 1
        except KeyError:
            lifes[link] = 1
        active_links.add(link)

    elif arr == -1:
        lifes[u] -= 1
        lifes[v] -= 1

        if lifes[u] <= 0:
            if u in active_nodes:
				active_nodes.remove(u)
        if lifes[v] <= 0:
            if v in active_nodes:
				active_nodes.remove(v)

        lifes[link] -= 1

        if lifes[link] <= 0:
            if link in active_links:
                active_links.remove(link)

# n = len(active_nodes)
# m = len(active_links)

# if n > 1:
    # print(str(old_t) + " " + str(n) + " " + str(float(2*m)/(n*(n-1))))
# else:
    # print(str(old_t) + " " + str(n) + " 0.0")