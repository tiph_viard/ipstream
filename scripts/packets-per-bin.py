import sys

old_t = 0
nb_pkts = 0

for line in sys.stdin:
	t = int(float(line.split(" ")[0]))

	if old_t != t and old_t != "":
		sys.stdout.write(str(old_t) + " " + str(nb_pkts) + "\n")
		old_t = t
		nb_pkts = 0
	nb_pkts += 1

sys.stdout.write(str(old_t) + " " + str(nb_pkts) + "\n")
