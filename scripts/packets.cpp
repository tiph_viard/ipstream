#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string>
#include <cassert>

#define MAX_NODES 1361343

// Packets class ?
int main() {
    float b = 0.0; float e = 0.0;
    int u = 0; int v = 0;
    //Read input
    for(std::string line; std::getline(std::cin, line);) {
        std::istringstream iss(line);
        iss >> b >> e >> u >> v;
        assert(u <= MAX_NODES && v <= MAX_NODES);
        printf("%f %f %d %d\n", b, e, u, v);
    }   
}
