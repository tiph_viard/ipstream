import sys
import pdb
import gzip


def time_sorted(infile):
    # Compute here pkts/s
    with gzip.open(infile) as f:
        old_t = 0
        pkt_count = 0
        f1 = open("packets-s", "w")
        for line in f:
            # pdb.set_trace()
            # print(str(line))
            contents = line.split(b' ')
            t = int(float(contents[0]))
            # u = int(contents[1])
            # v = int(contents[2])
            
            if t != old_t:
                f1.write(str(old_t) + " " + str(pkt_count) + "\n")
                sys.stdout.write(str(old_t) + " " + str(pkt_count) + "\n")
                pkt_count = 0
                old_t = t
            pkt_count += 1
        f1.write(str(old_t) + " " + str(pkt_count) + "\n")
        sys.stdout.write(str(old_t) + " " + str(pkt_count) + "\n")

def u_sorted(infile):
    # pkts/IP, pkts/IP/s
    with gzip.open(infile) as f:
        old_t = 0
        old_u = -1
        pkt_ip_count = 0
        pkt_ip_s_count = 0
        f1 = open("packets-ip", "w")
        f2 = open("packets-ip-s", "w")

        for line in f:
            contents = line.split(b' ')
            t = int(float(contents[0]))
            u = int(contents[1])
            
            if u != old_u and old_u != -1:
                f1.write(str(old_u) + " " + str(pkt_ip_count) + "\n")
                pkt_ip_count = 0
                f2.write(str(old_u) + " " + str(old_t) + " " + str(pkt_ip_s_count) + "\n")
                old_t = 0
                pkt_ip_s_count = 0
            
            if t != old_t:
                f2.write(str(old_u) + " " + str(old_t) + " " + str(pkt_ip_s_count) + "\n")
                old_t = t
                pkt_ip_s_count = 0

            pkt_ip_count += 1
            pkt_ip_s_count += 1
            old_u = u

        # Last IP
        if old_u != -1 and old_t != 0:
            f1.write(str(old_u) + " " + str(pkt_ip_count) + "\n")
            f2.write(str(old_u) + " " + str(old_t) + " " + str(pkt_ip_s_count) + "\n")

def uv_sorted(infile):
    # pkts/uv, pkts/uv/s
    with gzip.open(infile) as f:
        old_t = 0
        old_uv = ""
        pkt_uv_count = 0
        pkt_uv_s_count = 0
        f1 = open("packets-uv", "w")
        f2 = open("packets-uv-s", "w")
        for line in f:
            contents = line.split(b' ')
            t = int(float(contents[0]))
            u = int(contents[1].decode().strip())
            v = int(contents[2].decode().strip())

            if u < v:
                uv = str(u) + "-" + str(v)
            else:
                uv = str(v) + "-" + str(u)

            if uv != old_uv and old_uv != "":
                f1.write(str(old_uv) + " " + str(pkt_uv_count) + "\n")
                pkt_uv_count = 0
                f2.write(str(old_uv) + " " + str(t) + " " + str(pkt_uv_s_count) + "\n")
                old_t = 0
                pkt_uv_s_count = 0
            
            if t != old_t and old_t != 0:
                f2.write(str(old_uv) + " " + str(t) + " " + str(pkt_uv_s_count) + "\n")
                old_t = t
                pkt_uv_s_count = 0

            pkt_uv_count += 1
            pkt_uv_s_count += 1
            old_uv = uv

        # Last IP
        if old_uv != "" and old_t != 0:
            f1.write(str(old_uv) + " " + str(pkt_uv_count) + "\n")
            f2.write(str(old_uv) + " " + str(old_t) + " " + str(pkt_uv_s_count) + "\n")
            
def checks():
    """
        Runs through all files to make sanity checks.
    """
    pass

# Main
# time_sorted("../data/20130625.time-sort.1.gz")
# u_sorted("../data/20130625.u-sort.1.gz")
uv_sorted("../data/20130625.uv-sort.1.gz")

# time_sorted("./test.gz")
# u_sorted("./test.gz")
# uv_sorted("./test.gz")
