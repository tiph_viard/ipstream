\documentclass[10pt, conference, letterpaper]{IEEEtran}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage{graphicx}

\newtheorem{definition}{Definition}

\ifCLASSINFOpdf
  % \usepackage[pdftex]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../pdf/}{../jpeg/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\else
  % or other class option (dvipsone, dvipdf, if not using dvips). graphicx
  % will default to the driver specified in the system graphics.cfg if no
  % driver is specified.
  % \usepackage[dvips]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../eps/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.eps}
\fi

\hyphenation{}

\newcommand{\comment}[1]{\medskip {\bf #1}\medskip}


\begin{document}

%\title{Identifying roles in a network\\ from IP traffic time scales}

%\title{Temporal-Structural Density of Links i	n IP Traffic\\ as a Tool to Identify Roles in a Network}

\title{Shedding light on events in IP traffic with link streams}

\author{\IEEEauthorblockN{Tiphaine Viard, Audrey Wilmet, Matthieu Latapy, Cl\'{e}mence Magnien, Romain Fontugne, Johan Mazel, Kensuke Fukuda}
\IEEEauthorblockA{Sorbonne Universit\'es, UPMC Univ Paris 06, UMR 7606, LIP6, F-75005 \\
CNRS, UMR 7606, LIP6, F-75005, Paris, France\\
Email: firstname.name@lip6.fr}
\IEEEauthorblockA{Fukuda Lab\\
	National Institute of Informatics\\
	2-1-2, Hitotsubashi, Chiyoda-ku, Tokyo, Japan}}
\maketitle

\begin{abstract}

\end{abstract}

\section{Introduction}
\label{intro}

Measurement, analysis and modeling network traffic at IP level has now become a classical field in computer networking research~\cite{ip_measurement, ip_analysis, ip_modeling}. It relies on captures of traffic traces on actual networks, leading to huge series of packets sent by machines (identified by their IP adress) to others. It is therefore natural to see such data as graphs where nodes are IP adresses and links indicate that a packet exchange was observed between the two corresponding machines. One obtains this way large graphs which encode much information on the structure of observed exchanges, and network science is the natural framework for studying them \cite{Iliofotou:2009:EDG:1658939.1658967, Eberle:2007:ADD:1368018.1368024}.

One key feature of network traffic is its intense dynamics. It plays a crucial role for network optimization, fault/attack detection and fighting, and many other applications. As a consequence, much work is devoted to the analysis of this dynamics \cite{multiscale_ip_traffic, borgnat_signal,Guralnik:1999:EDT:312129.312190, graph_wavelets}. In network science, studying such dynamics means that one studies the dynamics of the associated graphs \cite{Broido01internettopology}. The most common graph approach relies on series of snapshots: for a given $\Delta$, one considers the graph $G_t$ induced by exchanges that occured in a time window from $t$ to $t+\Delta$, then the graphs $G_{t+\Delta}$, $G_{t+2\Delta}$, and so on \cite{graph_snapshots}. Many variants exist, but the baseline remains that one splits time into (possibly overlapping) slices of given (but possibly evolving) length $\Delta$ \cite{modeling_analysis_graphs}.

Obviously, a key problem with this approach is that one must choose appropriate values of $\Delta$: too small ones lead to trivial snapshots, while too large ones lead to important losses of information on the dynamics. In addition, appropriate values of $\Delta$ may vary over time, for instance because of day-night changes in activity. As a consequence, much work has been done to design methods for choosing and assessing choices in the value of $\Delta$ \cite{lamia_infocom,temp_scale_dynnet, thomas_LIP64707}. In \cite{temp_scale_dynnet, thomas_LIP64707, Hulten:2001:MTD:502512.502529}, the authors even propose methods to choose values of $\Delta$ that vary over time, or to consider non-contiguous time windows. In all situations, however, authors assume that merging all the events occurring at a same time is appropriate.

On the countrary, we argue that there are interactions in IP traffic that occur concurently but at different time scales, and that they should not be merged. For instance, users interacting with a system will have a faster dynamics than a backup service that automatically saves data every 24 hours, and a slower dynamics than a P2P system or a large file transfer between two machines. Likewise, attacks may have dynamics that distinguish them from legitimate traffic \cite{attack_behaviour}. This means that different parts of the traffic may have different appropriate values of $\Delta$, even though they occur at the same time (or in the same time window). These interactions are different in nature; they reflect different roles for involved nodes (like an end-user machine, or a backup server) that should be studied separately to accurately reflect the actual activity occurring in the network.

We propose in this paper an approach for doing so. It relies on a notion of $\Delta$-density that captures up to what point links appear {\em all the time} and/or all possible links between considered nodes occur {\em all the time} (Section~\ref{definitions}). To this regard, it may be seen as a generalization of classical graph density and its local version, clustering coefficient. We show how this notion may be used to identify one or several appropriate time scales for various parts of the traffic, and how mixing time and structure makes it possible to identify (groups of) machines playing specific roles in a network (Section~\ref{exp}). All along this paper, we illustrate and validate our approach using two real-world captures of traffic on a firewall between a local network and the internet. It consists of packets that were observed on the firewall in a time period of one month.

\section{Framework}

A \emph{bipartite link stream} $L = ([\alpha, \omega], \top,\bot, E)$ such that $[\alpha,\omega]$ is the time interval of the measurement, $\top$ and $\bot$ are sets of nodes such that $\top \cap \bot = \emptyset$, and $E \subseteq [\alpha, \omega]\times \top \times \bot$ is a set of links, \emph{i.e.} $(b,e,u,v)\in E$ means that nodes $u$ and $v$ are in contact from time $b$ to time $e$.

	\comment{Substreams, (induced graph)}

	\subsection{Degree}
	
	The neighborhood $N_t(v)$ of node $v\in V$ at time $t\in T$ is the set of nodes linked to $v$ at time $t$: $N_t(v) = \{u \in V, \exists (b,e,u,v) \in E, t \in [b,e]\}$. The degree $d_t(v)$ of $v$ at time $t$ is the size of its neighborhood: $d_t(v) = |N_t(v)|$.

The neighborhood $N_{t..t?}(v)$ of node $v\in V$ from time $t\in T$ to time $t' \in T$, $t' > t$ is the set of nodes linked to $v$ between $t$ and $t'$: $N_{t..t'}(v) = \{u \in V, \exists (b,e,u,v) \in E, [t,t']\cap [b,e] \ne \emptyset\}$. 

We define the degree of $v$ from $t$ to $t'$ as:
$$
d_{t..t'}(v)= \sum_{(b,e,u,w)\in E,w=v}  \frac{|[b,e]\cap [t,t']|}{t'-t} = \sum_{l\in L_{t..t'} (v)} \frac{\overline{l}}{t'-t}
$$

Notice that the degree of $v$ from $t$ to $t'$ is not the size of $N_{t..t'} (v)$. Indeed, the contribution
of each neighbor $u$ to the degree of $v$ is weighted by the duration of its links with $v$: $d_{t..t'}(v) = \sum{u\in N_{t..t'} (v)} \sum{(b,e,u,v)\in E} \frac{|[b,e]\cap [t,t']|}{t'-t}$.

Notice also that $d_{t..t'} (v)$ is the expected degree $d_t(v)$ at time $t$ when $t$ is chosen randomly in $[t,t']$.

%d

%|[b,e]\cap [t,t']|. (b,e,u,v)?E t??t
%?(G(Lt))dt
     
    By extension, we define the degree of $v$ in $L$ as $d(v) = d_{\alpha..\omega}(v)$:
    $$
		    d(v)= \sum_{u} \frac{\overline{l}}{\omega - \alpha} =\sum_{u} \frac{|\tau(u,v|}{\omega - \alpha}
	$$
	
	\subsection{Density}
	
	$$
		\delta(L) = \frac{2\cdot \sum_{u,v} \tau(u,v)}{|\top|\cdot|\bot|\cdot (\omega - \alpha)}
	$$
	
	In other words, it is the probability when one chooses two nodes at random and a time instant, that there is a link in $E$.
	
	\subsection{Clustering coefficient}
	
		In graphs, one definition of the clustering coefficient of a node $u\in V$ is the density of its neighborhood, where the neighborhood is the set of nodes $\{x\}$ such that for each $x$, there is an edge $(u,x)$ in the graph.
		
		We extend the neighborhood to link streams: 
	
	\subsection{Cliques}   

	Viard \emph{et al.} extend the defintion of cliques to link streams in \cite{tcs}. A clique is a couple $(X, [b,e])$ (def clique).
	
	We simply extend this definition of clique to bipartite link streams. 

	\subsection{Projection}

	Given a bipartite link stream $L = ([\alpha, \omega], \top, \bot, E)$, we define its $\top$-projection $L^{\top} = ([\alpha, \omega], \top, \{ ... \})$ and its $\bot$-projection $L^{\bot} = ([\alpha, \omega], \bot, \{ ... \})$. See figure for an example.

\section{Analysis of IP traffic}
	
	In order to validate our theoretical framework, we study events in IP traffic.
	
	\subsection{Dataset}
	
	\comment{Mettre avant les d�finitions ? pour parler du bipartisme. Et ajouter ici une section "MAWI traffic as a link stream" ?}
	
	We use a 24-hour long trace captured on June 26\textsuperscript{th}, 2012, as part of the "Day In The Life of the Internet" (DITL) initiative\cite{ditl}. It consists of $n$ packets captured using port mirrorring on a backbone router between \textsc{WIDE} (one of the japanese academic networks) and the rest of the world.
	
	Notice that due to the nature of the capture method, the data is instrinsically bipartite, \emph{i.e.} we only see packets between a machine in \textsc{WIDE} and the Internet (or the converse).
	
	\subsubsection{IP traffic as a link stream}
	
	Explain $\Delta$, equivalence between punctual simple stream with $\Delta$ and duration stream without $\Delta$, bipartite separation
	 
	
	\subsection{Methodology}
	
	Given the size of the data, some of the notions defined in section \ref{} are not readily computable. 
	
	Instead, our approach is to compute the simplest statistics on the whole stream (at most in $\mathcal{O}(E)$ time), such as the degrees. We then use these very basic statistics to isolate subsets of nodes  $X\subseteq \top\cup \bot$ over intervals of time $[b,e]$ that are worthy of interest, and extract the substream $L_{b..e}(X)$.
	
	\comment{Distribution des tailles de $X$, des dur�es $e-b$ extraites}
	
	$L_{b..e}(X)$ is of a much more reasonable size, we can be more ambitious on the computed notions.
	
	\comment{Discussion sur ce que l'on "manque" en ne regardant que des stats simples ?}
	
	
	\subsection{Results}

	\subsection{Cross-validation with MAWILab}
	
	\comment{Aim : find share of common events and new events unseen before, maybe with validation from JRK ?}
	
	The \emph{MAWILab}~\cite{mawilab} initiative intends at detecting and sharing attacks on the \emph{MAWI} dataset. 

	\section{Related Work}
	
	Short on event detection (signal, traffic matrix, etc.); develop on graph event detection
		
	\section{Conclusion}

\section{Acknowledgments}

This work is supported in part by the french Direction G�n�rale de l'Armement (DGA), by the means of a doctoral grant. It is also partly supported by the DynGraph grant from the Agence Nationale de la Recherche with reference ANR-10-JCJC-0202, and by the Request and CODDDE grants from the Agence Nationale de la Recherche.

\bibliographystyle{plain}
\bibliography{bare_conf}

\end{document}

