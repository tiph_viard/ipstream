import sys

prev_link = frozenset([])
buffer = []

for line in sys.stdin:
        contents = line.split(" ")
        b = float(contents[0])
        e = float(contents[1])
        u = contents[2]
        v = contents[3].strip()

        link = frozenset([u,v])
        # print(link, prev_link)
        # print(len(prev_link))
        if len(prev_link) == 2 and link != prev_link:
                for l in buffer:
                        sys.stdout.write("%f %f %s %s\n" % (l[0], l[1], l[2], l[3]))
                buffer = []

        if len(buffer) == 0:
                buffer.append([b,e,u,v])
        else:
                last = buffer[-1]
                if b <= last[1] and e > last[1]:
                        buffer[-1][1] = e
                elif b > last[1]:
                        buffer.append([b,e,u,v])
        prev_link = link

for l in buffer:
        sys.stdout.write("%f %f %s %s\n" % (l[0], l[1], l[2], l[3]))
