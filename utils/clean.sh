#!/bin/bash

data=$1
fname=`echo "$1"|cut -d"." -f1`
out=$2

zcat $1 | awk '{if($3<$4){print($1,$2,$3,$4);}else{print($1,$2,$4,$3)}}' | sort -T. -S5G -k3,3 -k4,4 -k1,1g -k2,2g | grep -v '^#' | python ./utils/clean.py | sort -T. -S5g -k1,1g -k3,3 -k4,4 
