# plot-dist.gp

print file
DIST_HEAD="./dists/"
CCDF_HEAD="./ccdfs/"

# PNG for quick PDF
set ter png

set out file."-dist-linlin.png"
unset log x
unset log y
plot DIST_HEAD.file.".dist" notitle

set out file."-dist-loglin.png"
set log x
unset log y
plot DIST_HEAD.file.".dist" notitle

set out file."-dist-linlog.png"
unset log x
set log y
plot DIST_HEAD.file.".dist" notitle

set out file."-dist-loglog.png"
set log x
set log y
plot DIST_HEAD.file.".dist" notitle



set out file."-ccdf-linlin.png"
unset log x
unset log y
plot CCDF_HEAD.file.".ccdf" notitle

set out file."-ccdf-loglin.png"
set log x
unset log y
plot CCDF_HEAD.file.".ccdf" notitle

set out file."-ccdf-linlog.png"
unset log x
set log y
plot CCDF_HEAD.file.".ccdf" notitle

set out file."-ccdf-loglog.png"
set log x
set log y
plot CCDF_HEAD.file.".ccdf" notitle

# EPS for manual inspection
set ter pos col

set out file."-dist-linlin.eps"
unset log x
unset log y
plot DIST_HEAD.file.".dist" notitle

set out file."-dist-loglin.eps"
set log x
unset log y
plot DIST_HEAD.file.".dist" notitle

set out file."-dist-linlog.eps"
unset log x
set log y
plot DIST_HEAD.file.".dist" notitle

set out file."-dist-loglog.eps"
set log x
set log y
plot DIST_HEAD.file.".dist" notitle



set out file."-ccdf-linlin.eps"
unset log x
unset log y
plot CCDF_HEAD.file.".ccdf" notitle

set out file."-ccdf-loglin.eps"
set log x
unset log y
plot CCDF_HEAD.file.".ccdf" notitle

set out file."-ccdf-linlog.eps"
unset log x
set log y
plot CCDF_HEAD.file.".ccdf" notitle

set out file."-ccdf-loglog.eps"
set log x
set log y
plot CCDF_HEAD.file.".ccdf" notitle
