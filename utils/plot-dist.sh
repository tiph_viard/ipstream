#!/bin/bash
# plot-dists.sh

filename=$1

mkdir $filename"-eps"

gnuplot -e "file='$filename'" plot-dist.gp
echo "Done making plots."

convert $filename-dist-linlin.png $filename-dist-loglin.png $filename-dist-linlog.png $filename-dist-loglog.png $filename-ccdf-linlin.png $filename-ccdf-loglin.png $filename-ccdf-linlog.png $filename-ccdf-loglog.png $filename.pdf
pdfnup --nup 4x2 $filename.pdf
echo "Done making compact PDF."

mv *.eps $filename"-eps/"
mv $filename-nup.pdf $filename.pdf
rm $filename-dist-linlin.png $filename-dist-loglin.png $filename-dist-linlog.png $filename-dist-loglog.png $filename-ccdf-linlin.png $filename-ccdf-loglin.png $filename-ccdf-linlog.png $filename-ccdf-loglog.png
echo "Done cleaning."
