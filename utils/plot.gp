# Standard plot file (nothing fancy)
set title tit;

set ter pos col;
set out path."/eps/".tit.".eps";

#load path."/ano-".tit.".gp";

set xlabel xlab font ",24";
set ylabel ylab font ",24";

plot "< cat ".path."/".tit." |grep glob" u 4:3 w l notitle;
