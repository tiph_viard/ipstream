# -*- coding: utf8 -*-
# Adapted for bipartite MAWI case.
# WIDE = even
# Non-WIDE = odd

import sys
import pdb

nodes = {}
cpt_wide = 0
cpt_non_wide = 1

for line in sys.stdin:
	if line.find('#') is -1:
		contents = line.split(" ")
		t = contents[0].strip()
		u = contents[1].strip()
		v = contents[2].strip()

		if nodes.get(u) is None:
			nodes[u] = cpt_wide
			sys.stderr.write(u + " " + str(cpt_wide) + "\n")
			cpt_wide += 1
		nodeu = nodes[u]

		if nodes.get(v) is None:
			nodes[v] = cpt_wide
			sys.stderr.write(v + " " + str(cpt_wide) + "\n")
			cpt_wide += 1
		nodev = nodes[v]

		sys.stdout.write(str(t) + " " + str(nodeu) + " " + str(nodev) + "\n")
	else:
		sys.stdout.write(line)
