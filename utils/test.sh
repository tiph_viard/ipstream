#!/bin/bash
cd $1
cp ~/Development/ipstream/utils/plot-dist.sh .
cp ~/Development/ipstream/utils/plot-dist.gp .
#export PATH=$PATH:"~/Development/ipstream/utils/"
for file in $(find . -maxdepth 1 -type f -not -name "*.pdf" -not -name "*.sh" -not -name "*.gp")
do
    bfile=$(basename $file)
    echo $bfile
    # Make dist and ccdf
    cat $bfile | awk '{print($NF)}'|dist > ./dists/$bfile.dist
    cat $bfile | awk '{print($NF)}'|dist | ccdf > ./ccdfs/$bfile.ccdf
    ./plot-dist.sh $bfile

done

rm plot-dist.sh
rm plot-dist.gp
